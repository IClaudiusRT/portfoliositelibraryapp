import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  backUpBook = [];

  STORAGE_KEY = 'local_book_ids';

  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) { }

  public storeOnLocalStorage(taskTitle: string):void {

    const currentBookList = this.storage.get(this.STORAGE_KEY) ||
    [];

    currentBookList.push({
      title: taskTitle, 
      isChecked: false
    });

    this.storage.set(this.STORAGE_KEY, currentBookList);

    console.log(this.storage.get(this.STORAGE_KEY) || 'Local storage is empty');


  }

  public removeLocalStorage() {
    this.storage.remove(this.STORAGE_KEY);
  }

  public getLocalList() {
    return this.storage.get(this.STORAGE_KEY)
  }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LibraryService {
  key = "AIzaSyBpvM56AEaxi-lCqy-XCaJt-7aLkUb1GRM";

  constructor(private http: HttpClient) { }

  get(queryField: string) {
    return this.http.get(`https://www.googleapis.com/books/v1/volumes?q=${queryField}&maxResults=9&keyes&key=${this.key}`)

  }

  getSingleBook(queryField: string) {
    return this.http.get(`https://www.googleapis.com/books/v1/volumes/${queryField}?key=${this.key}`)
  }

}

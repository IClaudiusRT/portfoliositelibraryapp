import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  debounceTime,
  distinctUntilChanged,
} from "rxjs/operators";
import { LibraryService } from '../library.service';
import { LocalStorageService } from '../local-storage.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  bookList = [];  //strings of ID

  items: any;
  loading;
  queryField: FormControl = new FormControl();

  constructor(private service: LibraryService, private localStorageService: LocalStorageService) { }

  ngOnInit() {
    this.loading = false;
    this.queryField.valueChanges
      .pipe(debounceTime(2000), distinctUntilChanged())
      .subscribe((query: any) => {
        let te = query.replace(/\s/g, "");
        if (te.length > 2) {
          this.service.get(query).subscribe((result: any) => {
            this.loading = true;
            setTimeout(() => {
              this.items = result.items;
              console.log(this.items);
            }, 3000);
          });
          console.log(query);
        }

        console.log(query);
      });
    console.log(this.queryField);
  }

  /*storeBook(idString: string) {
    this.bookList.push(idString);

    console.log(this.bookList);
    console.log(this.bookList.length);

  }*/

  storeOnLocal(iDstring: string){

  const newBookID = iDstring;
  this.localStorageService.storeOnLocalStorage(newBookID);

  }

  removeLocalData() {
    this.localStorageService.removeLocalStorage();
  }

}

import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { LibraryService } from '../library.service';
import { LocalStorageService } from '../local-storage.service';

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.css'],
})
export class LibraryComponent implements OnInit {
  items: any;
  personalBookList: any;
  singleBookObject: any;
  singleBookStringID: any;
  finalBook: any;

  booksInLibrary = [];
  tempBook: any;

  isTrue = false;
  //
  query: FormControl = new FormControl();

  constructor(
    private service: LibraryService,
    private localStorageService: LocalStorageService
  ) {}

  ngOnInit() {
          
  }

  getStoredList() {
    this.personalBookList = this.localStorageService.getLocalList();
    console.log(this.personalBookList);

    this.getStoredID();
  }

  getStoredID() {
    for (let i = 0; i < this.personalBookList.length; i++) {
      this.singleBookObject = this.personalBookList[i];

      this.singleBookStringID = Object.values(this.singleBookObject)[0];

      this.finalBook = this.service.getSingleBook(this.singleBookStringID);

      const obs = this.service.getSingleBook(this.singleBookStringID);
      obs.subscribe(result => this.booksInLibrary.push(result));
      
    }
    
  }

  openDetails(stringTitle: string) {
    for (let index = 0; index < this.booksInLibrary.length; index++) {
      const element = this.booksInLibrary[index];
      if (element.volumeInfo.title === stringTitle) {
        this.tempBook=element;
        !this.isTrue;
      } else {

      }      
    }
  }

  removeLocalData() {
    this.localStorageService.removeLocalStorage();
    
  }
  
}
